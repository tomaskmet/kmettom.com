var path = require('path')
var webpack = require('webpack')
// var autoprefixer = require('autoprefixer');
// var precss = require('precss');

const VersioningPlugin = require('versioning-webpack-plugin');
const WebpackMd5Hash = require('webpack-md5-hash');
var HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')

module.exports = {
  entry: './src/main.js',
  output: {
    path: path.resolve(__dirname, './dist'),
    publicPath: '/dist/',
    filename: process.env.NODE_ENV === 'production' ?  "build.[hash].js" : "build.js",
    // filename:  "build.[hash].js",
    // filename: "build.js",
  },
  devServer: {
    contentBase: path.join(__dirname, 'src'),
    // compress: true,
    // port: 9000
  },
  module: {
    loaders: [
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader'
      }
    ],
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {
            // Since sass-loader (weirdly) has SCSS as its default parse mode, we map
            // the "scss" and "sass" values for the lang attribute to the right configs here.
            // other preprocessors should work out of the box, no loader config like this necessary.
            'scss': 'vue-style-loader!css-loader!sass-loader',
            'sass': 'vue-style-loader!css-loader!sass-loader?indentedSyntax'
          },
          target: 'node-webkit',
          // other vue-loader options go here
        }
      },
      {
        test: /\.js$/,
        use: {
          loader: 'babel-loader',
          options: {
            cacheDirectory: true,
            presets: [require.resolve("babel-preset-es2015")]
          }
        },
        exclude: /node_modules/
        // loader: 'babel-loader',
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]'
        }
      }
    ]
  },
  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js'
    }
  },
  devServer: {
    historyApiFallback: true,
    noInfo: true
  },
  performance: {
    hints: false
  },
  plugins: [
    // new UglifyJsPlugin({
    //   // compress: {
    //   //   warnings: false
    //   // },
    //   // sourceMap: true
    // }),
    new VersioningPlugin({
        cleanup: true,                      // should it remove old files?
        basePath: '../',                     // manifest.json base path
        manifestFilename: 'versioning.json'   // name of the manifest file
    }),
    new WebpackMd5Hash(),
    new HtmlWebpackPlugin({
      template: 'src/index.html',
      filename: "../index.html"
    }),
  ],
  devtool: '#eval-source-map'
  // postcss: function () {
  //
  //     return {
  //         defaults: [autoprefixer, precss],
  //         cleaner:  [autoprefixer({ browsers: ['IE 10', 'IE 11', 'firefox 20', 'ios_saf 8.4', 'android 4.3'] })]
  //     };
  //
  // },
}

if (process.env.NODE_ENV === 'production') {
  module.exports.devtool = '#source-map'
  // http://vue-loader.vuejs.org/en/workflow/production.html
  module.exports.plugins = (module.exports.plugins || []).concat([
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    }),
    // new webpack.optimize.UglifyJsPlugin({
    //   sourceMap: true,
    //   compress: {
    //     warnings: false
    //   }
    // }),
    new webpack.LoaderOptionsPlugin({
      minimize: true
    })
  ])
}
