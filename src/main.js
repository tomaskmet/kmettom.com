import Vue from 'vue'
import App from './App.vue'

new Vue({
  el: '#appKmet',
  render: h => h(App)
});
